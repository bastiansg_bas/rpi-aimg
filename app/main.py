import sys
import logging

from rpi_aimg.joystick.joystick_listener import JoystickListener


logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)


joystick_listener = JoystickListener()
joystick_listener.start_listener()
logger.info('joystick_listener started')
