from setuptools import find_packages, setup

setup(
    name='rpi-aimg',
    packages=find_packages(),
    version='1.0.2',
    description='',
    author='Bas',
    author_email='bastiansg.bas@gmail.com',
    url='https://gitlab.com/bastiansg_bas/rpi-aimg',
    install_requires=[
        'pypianoroll==0.5.3',
        'requests==2.25.1',
        'networkx==2.5'
    ],
    package_data={'': ['*.yml', '*.yaml']},
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3"
    ]
)
