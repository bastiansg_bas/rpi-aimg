import os
import logging
import traceback

from requests import post
from itertools import cycle
from typing import Optional

from rpi_aimg.music_transformer.utils import encode_midi, decode_midi


logger = logging.getLogger(__name__)


MODEL_HOST = os.getenv('MODEL_HOST')


model_url = f'http://{MODEL_HOST}:8000/generate'
logger.info(f'model_url => {model_url}')
headers = {'content-type': 'application/json'}


class MusicTransformer():
    def __init__(self):
        self.problems = cycle(['conditioned', 'unconditioned'])
        self.problem = next(self.problems)

    def get_prediction(self, midi_path: str) -> Optional[str]:
        encoded_midi = encode_midi(midi_path)
        midi = {
            'generator': self.problem,
            'encoded_midi': encoded_midi
        }

        try:
            req = post(model_url, json=midi, headers=headers)

        except Exception:
            exception = traceback.format_exc()
            logger.error(f'get_prediction => {exception}')
            return

        status_code = req.status_code
        if status_code != 200:
            return

        mt_encoded_midi = req.json()['encoded_midi']
        decoded_midi_path = decode_midi(mt_encoded_midi)
        return decoded_midi_path

    def set_next_problem(self) -> str:
        self.problem = next(self.problems)
        logger.info(f'current_problem => {self.problem}')
        return self.problem
