import os
import uuid

from base64 import b64encode, b64decode


MIDI_OUTPUT_PATH = os.getenv('MIDI_OUTPUT_PATH', '/resources/midi')


def encode_midi(midi_path: str) -> str:
    with open(midi_path, 'rb') as f:
        content = f.read()
        encoded_midi = b64encode(content)
        encoded_midi = encoded_midi.decode('utf-8')
        return encoded_midi


def decode_midi(encoded_midi: str) -> str:
    decoded_midi = b64decode(encoded_midi)
    midi_file_name = uuid.uuid1().hex
    midi_file_name = f'generated_{midi_file_name}'
    midi_output_file = f'{MIDI_OUTPUT_PATH}/{midi_file_name}.mid'
    with open(midi_output_file, 'wb') as f:
        f.write(decoded_midi)
        return midi_output_file
