import os
import logging

import numpy as np

from typing import Optional
from pypianoroll import parse, Track, Multitrack


logger = logging.getLogger(__name__)


def parse_midi(midi_path: str) -> Optional[np.ndarray]:
    try:
        parsed_midi = parse(midi_path)

    except Exception:
        logger.error("can't parse with pypianoroll")
        return

    return parsed_midi


def write_pianoroll(pianoroll: np.ndarray, midi_output: str):
    t = Track(pianoroll)
    mt = Multitrack(tracks=[t])
    mt.write(midi_output)


def get_pianoroll(parsed_midi: Multitrack) -> np.ndarray:
    n_traks = len(parsed_midi.tracks)
    parsed_midi.merge_tracks(list(range(n_traks)))
    merged = parsed_midi.tracks[-1]
    merged.trim_trailing_silence()
    pianoroll = merged.pianoroll
    return pianoroll


def trim_silence(midi_path: str) -> str:
    midi_output = midi_path.replace('.mid', '_trimmed.mid')
    parsed_midi = parse_midi(midi_path)
    if not parsed_midi:
        return midi_path

    pianoroll = get_pianoroll(parsed_midi)
    fisrt_non_zero = np.transpose(np.nonzero(pianoroll))[0][0]
    trimmed = pianoroll[fisrt_non_zero:, :]
    write_pianoroll(trimmed, midi_output)
    return midi_output
