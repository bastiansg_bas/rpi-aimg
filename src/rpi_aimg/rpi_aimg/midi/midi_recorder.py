import os
import signal
import logging
import uuid
import subprocess

from glob import glob
from threading import Thread

from rpi_aimg.midi.pianoroll import trim_silence


logger = logging.getLogger(__name__)


MIDI_OUTPUT_PATH = os.getenv('MIDI_OUTPUT_PATH', '/resources/midi')


class MidiRecorder():
    def __init__(self):
        self.is_current_midi_generated = False
        self.pid = None

    def set_midi_port(self) -> bool:
        command = ['arecordmidi', '-l']
        output = subprocess.check_output(command)
        output = output.decode('utf-8')
        port_lines = output.split('\n')[:-1]
        if len(port_lines) < 3:
            return False

        port_line = port_lines[2]
        midi_port = port_line.split()[0]
        self.midi_port = midi_port
        logger.info(f'midi_port => {midi_port}')
        return True

    def recorder(self):
        midi_file_name = uuid.uuid1().hex
        self.midi_output_file = f'{MIDI_OUTPUT_PATH}/{midi_file_name}.mid'
        command = [
            'arecordmidi',
            '-p',
            self.midi_port,
            self.midi_output_file
        ]

        logger.info('starting midi_record')
        proc = subprocess.Popen(command, stdout=subprocess.PIPE)
        self.pid = proc.pid
        proc.communicate()

    def start_recorder(self):
        thread = Thread(target=self.recorder)
        thread.start()

    def stop_recorder(self) -> None:
        os.kill(self.pid, signal.SIGINT)
        logger.info('midi_record stoped')
        trimmed = trim_silence(self.midi_output_file)
        self.midi_output_file = trimmed
        self.pid = None

    def play_record(self):
        command = [
            'aplaymidi',
            self.midi_output_file,
            '--port',
            self.midi_port
        ]

        logger.info('playing midi')
        subprocess.run(command)

    def set_midi_output_file(self, midi_output_file: str):
        self.midi_output_file = midi_output_file

    def set_generated(self):
        self.is_current_midi_generated = False

    def change_record(self) -> str:
        if not self.is_current_midi_generated:
            self.is_current_midi_generated = True
            midi_files = glob(f'{MIDI_OUTPUT_PATH}/[!generated]*.mid')
            midi_type = 'user_input'

        else:
            self.is_current_midi_generated = False
            midi_files = glob(f'{MIDI_OUTPUT_PATH}/*.mid')
            midi_type = 'generated'

        midi_files.sort(key=os.path.getmtime)
        last_input_midi = midi_files[-1]
        self.midi_output_file = last_input_midi
        return midi_type
