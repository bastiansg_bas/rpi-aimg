import os

from time import sleep

from rpi_aimg.midi.midi_recorder import MidiRecorder
from rpi_aimg.display.sense_display import Display
from rpi_aimg.music_transformer.music_transformer import MusicTransformer


MAIN_IMAGE = os.getenv('MAIN_IMAGE', 'space_invader_1')
START_RECORDER_IMAGE = os.getenv('START_RECORDER_IMAGE', 'space_invader_2')
START_RECORDER_REFRESH_RATE = os.getenv('START_RECORDER_REFRESH_RATE', 0.5)
ERROR_IMAGE = os.getenv('ERROR_IMAGE', 'space_invader_3')
PREDICTION_IMAGE = os.getenv('PREDICTION_IMAGE', 'space_invader_1_c')
PREDICTION_REFRESH_RATE = os.getenv('PREDICTION_REFRESH_RATE', 2)
CONDITIONED_IMAGE = os.getenv('CONDITIONED_IMAGE', 'space_invader_4')
UNCONDITIONED_IMAGE = os.getenv('UNCONDITIONED_IMAGE', 'space_invader_5')
GENERATED_IMAGE = os.getenv('GENERATED_IMAGE', 'arrow_right')
USER_INPUT_IMAGE = os.getenv('USER_INPUT_IMAGE', 'arrow_left')
PLAY_MIDI_IMAGE = os.getenv('PLAY_MIDI_IMAGE', 'space_invader_1_b')


image_map = {
    'conditioned': CONDITIONED_IMAGE,
    'unconditioned': UNCONDITIONED_IMAGE,
    'generated': GENERATED_IMAGE,
    'user_input': USER_INPUT_IMAGE
}


display = Display()
midi_recorder = MidiRecorder()
music_transformer = MusicTransformer()


def set_fast_image(image: str):
    display.set_image(image)
    sleep(0.5)
    display.set_image(MAIN_IMAGE)


def init_action():
    display.set_image(MAIN_IMAGE)


def start_recorder_action():
    midi_port_seted = midi_recorder.set_midi_port()
    if not midi_port_seted:
        set_fast_image(ERROR_IMAGE)
        return

    display.start_intermittent_image(
        START_RECORDER_IMAGE,
        START_RECORDER_REFRESH_RATE
    )

    midi_recorder.start_recorder()


def play_midi_action():
    midi_port_seted = midi_recorder.set_midi_port()
    if not midi_port_seted:
        set_fast_image(ERROR_IMAGE)
        return

    display.start_color_cycle(PLAY_MIDI_IMAGE)
    midi_recorder.play_record()
    display.stop_color_cycle()
    display.set_image(MAIN_IMAGE)


def stop_recorder_action():
    if not midi_recorder.pid:
        set_fast_image(ERROR_IMAGE)
        return

    midi_recorder.stop_recorder()
    display.stop_intermittent_image()
    display.set_image(MAIN_IMAGE)


def prediction_action():
    display.start_intermittent_image(
        PREDICTION_IMAGE,
        PREDICTION_REFRESH_RATE
    )

    out_file = midi_recorder.midi_output_file
    generated_midi_path = music_transformer.get_prediction(out_file)

    if not generated_midi_path:
        display.stop_intermittent_image()
        set_fast_image(ERROR_IMAGE)
        return

    midi_recorder.set_generated()
    display.stop_intermittent_image()
    display.set_image(MAIN_IMAGE)
    midi_recorder.set_midi_output_file(generated_midi_path)
    play_midi_action()


def set_problem_action():
    problem = music_transformer.set_next_problem()
    problem_image = image_map[problem]
    set_fast_image(problem_image)


def change_record_action():
    midi_type = midi_recorder.change_record()
    midi_type_image = image_map[midi_type]
    set_fast_image(midi_type_image)


action_map = {
    "init": init_action,
    "start_recorder": start_recorder_action,
    "stop_recorder": stop_recorder_action,
    "play_midi": play_midi_action,
    "get_prediction": prediction_action,
    "set_problem": set_problem_action,
    "change_record": change_record_action
}
