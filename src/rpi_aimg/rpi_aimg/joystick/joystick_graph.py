import os
import json
import logging

from typing import Optional
from networkx import DiGraph, set_edge_attributes


logger = logging.getLogger(__name__)


JOYSTICK_GRAPH_PATH = os.getenv(
    'JOYSTICK_GRAPH_PATH',
    '/resources/joystick-graph.json'
)

START_NODE = os.getenv('START_NODE', 'init')


class JoystickGraph(DiGraph):
    def __init__(self):
        DiGraph.__init__(self)
        self.init_graph()
        self.current_node = START_NODE

    def load_graph_rels(self) -> dict:
        with open(JOYSTICK_GRAPH_PATH, 'r') as f:
            content = f.read()
            graph_rels = json.loads(content)
            return graph_rels

    def init_graph(self):
        graph_rels = self.load_graph_rels()
        for rel in graph_rels:
            source = rel['source']
            target = rel['target']
            props = rel['rel_props']
            self.add_edge(source, target)
            set_edge_attributes(self, {(source, target): props})

    def get_next_node(self, event: str) -> Optional[str]:
        successors = self.successors(self.current_node)
        edges = map(lambda x: {
            'edge': self.edges[self.current_node, x],
            'next_node': x}, successors
        )

        edges = filter(lambda x: x['edge']['event'] == event, edges)
        edges = list(edges)
        if not edges:
            return

        edge = edges[0]
        change_state = edge['edge']['change_state']
        next_node = edge['next_node']
        if change_state:
            self.current_node = next_node

        return next_node
