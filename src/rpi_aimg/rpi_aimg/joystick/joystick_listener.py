import logging

from threading import Thread
from sense_hat import SenseHat


from rpi_aimg.joystick.joystick_graph import JoystickGraph
from rpi_aimg.joystick.joystick_actions import action_map


logger = logging.getLogger(__name__)


class JoystickListener(SenseHat):
    def __init__(self):
        SenseHat.__init__(self)
        self.stick_graph = JoystickGraph()

    def run_node_action(self, node: str):
        action = action_map[node]
        action()

    def stick_listener(self):
        self.active = True
        current_node = self.stick_graph.current_node
        self.run_node_action(current_node)

        while self.active:
            event = self.stick.wait_for_event(emptybuffer=True)
            event = f'{event.action}_{event.direction}'
            next_node = self.stick_graph.get_next_node(event)
            if not next_node:
                continue

            logger.info(f'event => {event}')
            logger.info(f'current_node => {self.stick_graph.current_node}')
            self.run_node_action(next_node)

    def start_listener(self):
        thread = Thread(target=self.stick_listener)
        thread.start()

    def stop_listener(self):
        self.active = False
