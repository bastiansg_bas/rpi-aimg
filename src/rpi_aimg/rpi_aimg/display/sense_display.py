import os

from time import sleep
from sense_hat import SenseHat
from threading import Thread, Lock

from rpi_aimg.display.utils import load_images, next_color


IMAGES_FILE_PATH = os.getenv('IMAGES_FILE_PATH', '/resources/images/images.h5')
ROTATION = os.getenv('ROTATION', 180)


class Display(SenseHat):

    def __init__(self):
        SenseHat.__init__(self)
        self.mutex = Lock()
        self.set_rotation(int(ROTATION))
        self.images = load_images(IMAGES_FILE_PATH)

    def color_cycle(self, image_mask: str):
        self.mutex.acquire()
        r, g, b = (255, 0, 0)
        image_mask = self.images[image_mask]
        image_mask[image_mask > 0] = 1
        while self.color_cycle_run:
            r, g, b = next_color(r, g, b)
            image = image_mask * [r, g, b]
            self.set_pixels(image)

        self.clear()
        self.mutex.release()

    def intermittent_image(
            self, image_name: str, refresh_rate: float):
        self.mutex.acquire()
        while self.intermittent_image_run:
            self.set_pixels(self.images[image_name])
            sleep(refresh_rate)
            self.clear()
            sleep(refresh_rate)

        self.mutex.release()

    def start_intermittent_image(
            self, image_name: str, refresh_rate: float):
        self.intermittent_image_run = True
        thread = Thread(
            target=self.intermittent_image,
            args=(image_name, refresh_rate)
        )

        thread.start()

    def stop_intermittent_image(self):
        self.intermittent_image_run = False

    def start_color_cycle(self, image_mask: str):
        self.color_cycle_run = True
        thread = Thread(
            target=self.color_cycle,
            args=(image_mask, )
        )

        thread.start()

    def stop_color_cycle(self):
        self.color_cycle_run = False

    def set_image(self, image_name: str):
        self.mutex.acquire()
        self.set_pixels(self.images[image_name])
        self.mutex.release()
