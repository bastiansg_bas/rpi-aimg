PROYECT = rpi-amg
JUPYTER_IMAGE = $(PROYECT)-jupyter
APP_IMAGE = $(PROYECT)-app
PIP_IMAGE = $(PROYECT)-pip

env-setup:
	pipenv install --skip-lock

app-build:
	docker build -t $(APP_IMAGE) \
	-f ./build/app/Dockerfile .

app-run: check-model-host
	docker run -it --rm \
	--privileged --network host \
	-e MODEL_HOST=$(MODEL_HOST) \
	-v $(PWD)/resources:/resources \
	$(APP_IMAGE)

jupyter-build:
	docker build -t $(JUPYTER_IMAGE) \
	-f ./build/jupyter/Dockerfile .

jupyter-run: check-display_url
	docker run -it --rm \
	--privileged --network host \
	-e DISPLAY_URL=$(DISPLAY_URL) \
	-v $(PWD)/notebooks:/jupyter \
	-v $(PWD)/src:/src \
	-v $(PWD)/resources:/resources \
	$(JUPYTER_IMAGE)

pip-build:
	docker build -t $(PIP_IMAGE) -f ./build/pip-packages/Dockerfile .

pip-publish: pip-build
	docker run -it --rm $(PIP_IMAGE)

check-model-host:
ifndef MODEL_HOST
	$(error parameter MODEL_HOST is required)
endif

check-display_url:
ifndef DISPLAY_URL
	$(error parameter DISPLAY_URL is required)
endif
