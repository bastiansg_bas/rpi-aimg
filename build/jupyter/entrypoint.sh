#!/bin/env sh

pip install --no-deps -e /src/rpi_aimg

jupyter-lab --ip=0.0.0.0 --allow-root --no-browser --NotebookApp.custom_display_url=http://${DISPLAY_URL}:8888
